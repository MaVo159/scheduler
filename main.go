package scheduler

import "sync"
import "fmt"

type Scheduler struct {
	lock        sync.Mutex
	done        sync.Mutex
	waitingJobs uint64
	runningJobs uint64
	totalJobs   uint64
	freeSlots   uint64
	totalSlots  uint64
	signal      chan struct{} //signal for waiting jobs
}

func (s *Scheduler) Schedule(delta uint64) {
	s.lock.Lock()
	s.totalJobs += delta
	if s.totalJobs == delta {
		s.done.Lock()
	}
	s.lock.Unlock()
}

func (s *Scheduler) AddSlots(delta uint64) {
	if delta == 0 {
		return
	}
	s.lock.Lock()
	s.freeSlots += delta
	s.totalSlots += delta
	for s.waitingJobs > 0 && s.freeSlots > 0 {
		s.waitingJobs--
		s.runningJobs++
		s.freeSlots--
		s.signal <- struct{}{}
	}
	s.lock.Unlock()
}

func (s *Scheduler) Ready() {
	s.lock.Lock()
	if s.freeSlots > 0 {
		s.freeSlots--
		s.runningJobs++
		s.lock.Unlock()
	} else {
		s.waitingJobs++
		if s.signal == nil {
			s.signal = make(chan struct{})
		}
		sig := s.signal
		s.lock.Unlock()
		<-sig
	}
}

func (s *Scheduler) Done(delta uint64) {
	if delta == 0 {
		return
	}
	s.lock.Lock()
	s.freeSlots += delta
	s.runningJobs -= delta
	s.totalJobs -= delta
	for s.waitingJobs > 0 && s.freeSlots > 0 {
		s.waitingJobs--
		s.runningJobs++
		s.freeSlots--
		s.signal <- struct{}{}
	}
	if s.totalJobs == 0 {
		s.done.Unlock()
	}
	s.lock.Unlock()
}

func (s *Scheduler) Wait() {
	s.done.Lock()
	s.done.Unlock()
}

func (s *Scheduler) Debug() {
	s.lock.Lock()
	fmt.Println("Unfinished jobs:", s.totalJobs)
	fmt.Println("     Waiting   :", s.waitingJobs)
	fmt.Println("     Running   :", s.runningJobs)
	fmt.Println("Total slots    :", s.totalSlots)
	fmt.Println("     Free      :", s.freeSlots)
	s.lock.Unlock()
}
